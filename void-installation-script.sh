#!/usr/bin/env sh

sudo xbps-install -y void-repo-nonfree
sudo xbps-install -y void-repo-multilib
sudo xbps-install -y void-repo-multilib-nonfree
sudo xbps-install -Syu
sudo xbps-install -y xorg
sudo xbps-install -y firefox
sudo xbps-install -y alacritty
sudo xbps-install -y picom
sudo xbps-install -y vim
sudo xbps-install -y pcmanfm
sudo xbps-install -y awesome

git clone https://gitlab.com/jacinthsamuel/dotfiles
git clone https://gitlab.com/jacinthsamuel/dwm
git clone https://gitlab.com/jacinthsamuel/dmenu

cp ~/dotfiles/.bashrc ~/
mkdir ~/.config
cp -r ~/dotfiles/.config/* ~/.config/

sudo xbps-install -y lxappearance
sudo xbps-install -y arandr
sudo xbps-install -y blueman
sudo xbps-install -y bluez
sudo xbps-install -y breeze-amber-cursor-theme
sudo xbps-install -y breeze-gtk
sudo xbps-install -y breeze-icons
sudo xbps-install -y cantarell-fonts
sudo xbps-install -y cups
sudo xbps-install -y cups-filters
sudo xbps-install -y elogind
sudo xbps-install -y emacs-gtk3
sudo xbps-install -y exa
sudo xbps-install -y fish-shell
sudo xbps-install -y flameshot
sudo xbps-install -y font-adobe-source-code-pro
sudo xbps-install -y font-fantasque-sans-ttf
sudo xbps-install -y font-fira-otf
sudo xbps-install -y font-fira-ttf
sudo xbps-install -y font-firacode
sudo xbps-install -y font-go-ttf
sudo xbps-install -y font-hack-ttf
sudo xbps-install -y font-hermit-otf
sudo xbps-install -y font-ibm-plex-otf
sudo xbps-install -y font-ibm-plex-ttf
sudo xbps-install -y font-libertine-otf
sudo xbps-install -y font-libertine-ttf
sudo xbps-install -y font-manjari
sudo xbps-install -y font-material-design-icons-ttf
sudo xbps-install -y font-sil-annapurna
sudo xbps-install -y font-sony-misc
sudo xbps-install -y font-sourcecodepro
sudo xbps-install -y fontmanager
sudo xbps-install -y fonts-croscore-ttf
sudo xbps-install -y fonts-droid-ttf
sudo xbps-install -y fonts-nanum-ttf
sudo xbps-install -y fonts-nanum-ttf-extra
sudo xbps-install -y fonts-robot-ttf
sudo xbps-install -y galculator
sudo xbps-install -y gimp
sudo xbps-install -y google-fonts
sudo xbps-install -y gvfs-mtp
sudo xbps-install -y hblock
sudo xbps-install -y hplip
sudo xbps-install -y indic-otf
sudo xbps-install -y kvantum
sudo xbps-install -y libX11-devel
 sudo xbps-install -y libXft-devel
 sudo xbps-install -y libXinerama-devel
 sudo xbps-install -y liberation-fonts
 sudo xbps-install -y libreoffice
 sudo xbps-install -y libvirt
 sudo xbps-install -y mpv
 sudo xbps-install -y nerd-fonts
 sudo xbps-install -y nerd-fonts-otf
 sudo xbps-install -y nerd-fonts-ttf
 sudo xbps-install -y network-manager-applet
 sudo xbps-install -y nomacs
 sudo xbps-install -y noto-fonts-cjk
 sudo xbps-install -y noto-fonts-emoji
 sudo xbps-install -y noto-fonts-ttf
 sudo xbps-install -y noto-fonts-ttf-extra
 sudo xbps-install -y nvidia470
 sudo xbps-install -y okular
 sudo xbps-install -y p7zip
 sudo xbps-install -y parcellite
 sudo xbps-install -y pavucontrol
 sudo xbps-install -y pcmanfm
 sudo xbps-install -y pfetch
 sudo xbps-install -y picom
 sudo xbps-install -y pulseaudio
 sudo xbps-install -y qbittorrent
 sudo xbps-install -y qemu
 sudo xbps-install -y qt5ct
 sudo xbps-install -y qutebrowser
 sudo xbps-install -y rofi
 sudo xbps-install -y simple-scan
 sudo xbps-install -y source-sans-pro
 sudo xbps-install -y starship
 sudo xbps-install -y system-config-printer
 sudo xbps-install -y telegram-desktop
 sudo xbps-install -y terminus-font
 sudo xbps-install -y torbrowser-launcher
 sudo xbps-install -y ttf-ubuntu-font-family
 sudo xbps-install -y vim
 sudo xbps-install -y virt-manager
 sudo xbps-install -y virt-manager-tools
 sudo xbps-install -y vlc
 sudo xbps-install -y volctl
 sudo xbps-install -y xterm
 sudo xbps-install -y xinit

touch ~/.xinitrc
echo exec dbus-run-session awesome | tee ~/.xinitrc

cd ~/dwm
sudo make install

cd ~/dmenu
sudo make install

cd

echo QT_QPA_PLATFORMTHEME="qt5ct" | sudo tee /etc/environment

sudo usermod -aG libvirt,bluetooth,lpadmin $USER

sudo ln -s /etc/sv/{elogind,lightdm,dbus,bluetoothd,libvirtd,virtlockd,virtlogd} /var/service
startx
